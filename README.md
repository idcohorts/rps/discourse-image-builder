# RPS Discourse Image Builder

This repository is used to create a discourse image so we don't need to wait for the discourse deploy which takes long and to be able to version pin discourse reliably.

We wrote a script that uses the [discourse_docker repo](htps://github.com/discourse/discourse_docker) to build the image with the stable version in the most general way possible.

There is also a docker compose file that brings up the databases required for building and the discourse image for testing and runs it for local development. This can run on a GitLab runner with the `shell` executor, but you need a system with a `docker-compose` version that supports profiles.

## Approach

The build script should be run within our CI/CD pipelines so we can easily update the version so Further adjustments are done in different repos with Dockerfiles that are based on the image created by this repo.

## Develop

Run the build pipeline:

```
./build.sh
```

## Background

Discourse is a forum software that is used by the RPS. It is a very nice piece of software, but it is not very easy to deploy and to version pin. This repository is used to create a docker image that can be used to deploy discourse.

The problem is that discourse has a very unique way of building their docker images. They expect you to build the image on the target machine with their [discourse_docker repo](https://github.com/discourse/discourse_docker).

There have been a lengthy discussions about this on the discourse forum:

- https://meta.discourse.org/t/using-a-launcher-built-docker-image-in-docker-compose/105804
- https://meta.discourse.org/t/community-supported-official-docker-image/83768
- https://meta.discourse.org/t/community-supported-official-docker-image/83768

TL;DR: The discourse lead developers refuse to support a public docker image that can be used to deploy discourse. They want to keep the discourse_docker repo as the only official way to deploy discourse.

The main disadvatages for their approach are:

- It is not possible, from our experience, to version pin discourse reliably.
- It takes a long time to build the image and when the image is built the service is not available. Also discourse has the longest DevOps iteration cycle of all the services we support.
- Databases are managed in a different way than for usual docker based projects.
- The official Discourse deployment story is incompatible with OCI based development workflows and deployments, like Kubernetes or even Docker-Compose.
- The discourse_docker launcher makes a lot of assumptions about the environment it is running in. For example it refuses to run on rootless Podman with an error about missing storage volumes without being able to bypass this with an argument or env var.

I wrote a post about this repository on the discourse forum: https://meta.discourse.org/t/discourse-image-builder/261857

## Symbolbild

![xkcd compile time](https://imgs.xkcd.com/comics/compiling.png)

![Symbolbild](./symbolbild.jpg)

