#!/bin/bash

# let bash fail when the commands fail
set -e

# Make sure the database containers are down and the volumes are deleted before building
echo "Removing database containers and volumes to make sure we start with a clean slate"
docker-compose --project-name discourse-image-builder --profile databases down -v

# Bring up the database containers
echo "Starting database containers"
docker-compose --project-name discourse-image-builder --profile databases up -d

# copy the container config in the containers directory
echo "Copying container config"
cp discourse_web.yaml discourse_docker/containers/discourse-web.yml
chmod o-rwx discourse_docker/containers/discourse-web.yml

# Build the web container
echo "Building web container"
cd discourse_docker
./launcher rebuild discourse-web
cd ..
echo "... success"

# Remove the build container
echo "Removing build container"
docker rm -f discourse-web

# Bring down the database containers again
echo "Stopping database containers"
docker-compose --project-name discourse-image-builder --profile databases down -v
